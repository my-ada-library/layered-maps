with Ada.Containers.Indefinite_Holders;
with Ada.Containers.Indefinite_Hashed_Maps;
with Ada.Containers.Indefinite_Vectors;
with Ada.Containers.Vectors;
with Ada.Finalization;

use Ada;

--
-- ## What is this?
--
-- This package implements a generic "symbol table," that is a structure that
-- maps symbol "names" (a string) to symbol values.
--
-- The peculiarity that differentiate the symbol tables defined in this
-- package from an ordirary `Hashed_Maps.Map` is the possibility of
-- having different namespaces, possibly nested.
--
-- ### Data model
--
-- The abstract model is as follows.
-- * The symbol table contains one or more _namespace_
-- * A newly created table has only one Layer: the _root namespace_
--   that contains all the globally visible symbols
-- * At any time there is a _current_ Layer
-- * New namespaces can be created as _children_ of an existing namespace.
--   Two common choices for the parent Layer are
--   * The current Layer (like using a `declare .. begin .. end` in Ada)
--   * The root Layer (like when a new procedure is defined)
-- * When a symbol is searched  fore, first it is searched the current
---  Layer.  If the symbol is not found, it is searched in the parent,
--   grand-parent and so on... until the root Layer is reached.
-- * When a new Layer is created it becomes the current one;
--   when the Layer is closed, the previous current namespace is selected.
--
-- It turns out that namespaces are organized in two structures
--
-- * They are organized as a tree (having the blobal namspace as root)
--   according to the child-parent relationship
-- * They are organized as a stack whose top is the current Layer.
--   New namespaces are pushed to the top and they are popped when
--   they are closed.
--
-- ### Writing in the table
--
-- There are three operation to write in the table
--
-- * `Create` it creates a new entry in a table, possibly *empty* (that
--   is, the key is in the table, but no Element is associated).  This
--   function does not tranverse the map tree (it seems to me that it
--   it would not make any sense).  The entry must not exist in the table.
--   It can return the cursor to the new entry.
--
-- * `Update` it writes a Element in the table. The key must exist and
--   the tree is not tranversed.  It can accepts also a cursor.
--
-- * `Put` it looks in the tree for the required `key` (unless Local_Only
--   flag is specified).  If it finds it, it makes an `Update`, otherwise
--   it `Create` it in the last map searched (which is necessarily the root
--   the specified table, depending on the Local_Only flag) followed by
--   an `Update`
--
-- ### Cursors
--
-- When the table is queried it returns a `Cursor` that "points to" the
-- found element (or it assumes the special Element `No_Element`).  This
-- is similar to the behaviour of standard Ada containers.
--
-- The main difference with the usual `Cursor` is that the cursor of
-- this structure is "stable" with respect to tampering; that is, it
-- remains valid even if new elements and/or new namespaces are added
-- (I do not think this is guaranteed with the standard containers).
-- Only removing the Layer that contains the entry pointed by the cursors
-- invalidates the cursor (obviously).
--

generic
   type Key_Type (<>) is private;
   type Element_Type (<>) is private;

   with function Hash (Key : Key_Type) return Ada.Containers.Hash_Type;
   with function Equivalent_Names (X, Y : Key_Type) return Boolean;

   -- Used in some debug context and to generate exception messages
package Layered_Maps.Hashed_Maps is
   type Map is
     new Finalization.Limited_Controlled
   with
     private;

   type Layer_ID is private;
   No_Layer : constant Layer_ID;


   function Copy_Globals (T : Map) return Map;

   function Root (T : Map) return Layer_ID;
   -- Return the global Layer of the table

   function Current_Layer (T : Map) return Layer_ID;
   -- Return the current Layer

   function Parent_Of (T     : Map;
                       Layer : Layer_ID) return Layer_ID
     with Post => (if Layer = T.Root then Parent_Of'Result = No_Layer);
   -- Return the parent of a given Layer

   function Is_Ancestor (Ancestor : Layer_ID;
                         Descendant : Layer_ID)
                         return Boolean;

   procedure Open_New_Layer (Table  : in out Map;
                             Parent : Layer_ID)
     with
       Pre => Parent /= No_Layer,
       Post => Parent_Of (Table, Table.Current_Layer) = Parent;
   -- Create a new Layer with the given parent


   procedure Open_New_Layer (Table     : in out Map;
                             Parent    : Layer_ID;
                             New_Layer : out Layer_ID)
     with
       Pre => Parent /= No_Layer,
       Post => Parent_Of (Table, Table.Current_Layer) = Parent;
   -- Create a new Layer with the given parent and return the
   -- ID of the new layer


   procedure Close_Current_Layer (Table : in out Map)
     with
       Pre => Table.Current_Layer /= Table.Root;

   type Cursor is private;
   No_Element : constant Cursor;

   function Layer_Of (Pos : Cursor) return Layer_ID;

   function Is_Stale (Pos : Cursor) return Boolean;
   -- A cursor is stale when it refers to a layer that has been
   -- deleted.  Stale cursors should not lay around, but never
   -- say never...

   function Has_Value (Pos : Cursor) return Boolean
     with Pre => not Is_Stale (Pos);
   --
   -- Note that Has_Value(Pos) is more restrictive than Pos /= No_Element.
   --
   -- Pos could point to an entry that was allocated but has no Element
   -- yet.  Think about declaring a variable without initializing it.
   --
   -- Of course, if Has_Value(Pos), then it must be Pos /= No_Element.
   --

   function Element (Pos : Cursor) return Element_Type
     with Pre => Has_Value (Pos);


   function Key (Pos : Cursor) return Key_Type
     with Pre => not Is_Stale (Pos) and Pos /= No_Element;

   function Image (X : Cursor) return String;
   -- Return a printable representation of a cursor. Useful
   -- mostly for debugging



   function Find (Table      : Map;
                  Name       : Key_Type;
                  In_Layer   : Layer_ID;
                  Local_Only : Boolean := False)
                  return Cursor
     with
       Post =>
         Find'Result = No_Element
         or else
           (
              Hashed_Maps.Key (Find'Result) = Name
            and
              Is_Ancestor (Ancestor   => Layer_Of (Find'Result),
                           Descendant => In_Layer)
            and
              (if Local_Only then Layer_Of (Find'Result) = In_Layer)
           );
   -- Search for Key in the table, starting from the specified layer and
   -- moving toward the root.  If Local_Only is true, only the specified
   -- layer is searched.

   function Find (Table      : Map;
                  Name       : Key_Type;
                  Local_Only : Boolean := False)
                  return Cursor
   is (Table.Find (Name       => Name,
                   Local_Only => Local_Only,
                   In_Layer   => Table.Current_Layer));
   -- Syntactic sugar.  The layer defaults to the current one.


   function Contains (Table      : Map;
                      Name       : Key_Type;
                      In_Layer   : Layer_ID;
                      Local_Only : Boolean := False)
                      return Boolean
   is (Table.Find (Name       => Name,
                   Local_Only => Local_Only,
                   In_Layer   => In_Layer) /= No_Element);
   -- Return true if Key is stored in the specified layer or one of its
   -- ancestors.  If Local_Only is True, only the specified layer is
   -- searched.

   function Contains (Table      : Map;
                      Name       : Key_Type;
                      Local_Only : Boolean := False)
                      return Boolean
   is (Table.Find (Name, Local_Only) /= No_Element);
   -- Syntactic sugar.  The layer defaults to the current one.

   function Contains (Layer : Layer_ID;
                      Name  : Key_Type)
                      return Boolean;
   -- Search only the specified layer for the key.


   procedure Create
     (Table         : in out Map;
      Name          : Key_Type;
      In_Layer      : Layer_ID;
      Position      : out Cursor)
     with
       Pre =>
         not Table.Contains (Name, In_Layer, True),
     Post =>
       Table.Find (Name, In_Layer, True) = Position
     and Table.Current_Layer = Table.Current_Layer'Old
     and Position /= No_Element
     and not Has_Value (Position);



   procedure Create
     (Table         : in out Map;
      Name          : Key_Type;
      In_Layer      : Layer_ID)
     with
       Pre =>
         not Table.Contains (Name, In_Layer, True),
     Post =>
       Table.Current_Layer = Table.Current_Layer'Old
       and Table.Contains (Name, In_Layer, True)
     and not Has_Value (Table.Find (Name, In_Layer, True));


   procedure Create
     (Table         : in out Map;
      Name          : Key_Type;
      Position      : out Cursor)
     with
       Pre =>
         not Table.Contains (Name, Table.Current_Layer, True),
     Post =>
       Table.Find (Name, Table.Current_Layer, True) = Position
     and Table.Current_Layer = Table.Current_Layer'Old
     and Position /= No_Element
     and not Has_Value (Position);


   procedure Create
     (Table         : in out Map;
      Name          : Key_Type)
     with
       Pre =>
         not Table.Contains (Name, Table.Current_Layer, True),
     Post =>
       Table.Current_Layer = Table.Current_Layer'Old
       and Table.Contains (Name, Table.Current_Layer, True)
     and not Has_Value (Table.Find (Name, Table.Current_Layer, True));



   procedure Create (Table         : in out Map;
                     Name          : Key_Type;
                     Initial_Value : Element_Type)
     with
       Pre =>
         not Table.Contains (Name, Table.Current_Layer, True),
     Post =>
       Table.Contains (Name, Table.Current_Layer, True)
     and
       Table.Current_Layer = Table.Current_Layer'Old
       and
         Element (Table.Find (Name, Table.Current_Layer, True)) = Initial_Value;

   procedure Create (Table         : in out Map;
                     Name          : Key_Type;
                     Initial_Value : Element_Type;
                     Position      : out Cursor)
     with
       Pre =>
         not Table.Contains (Name, Table.Current_Layer, True),
     Post =>
       Table.Contains (Name, Table.Current_Layer, True)
     and Table.Current_Layer = Table.Current_Layer'Old
     and Table.Find (Name) = Position
     and Has_Value (Position)
     and Element (Position) = Initial_Value;

   procedure Create
     (Table         : in out Map;
      Name          : Key_Type;
      In_Layer      : Layer_ID;
      Initial_Value : Element_Type;
      Position      : out Cursor)
     with
       Pre =>
         not Table.Contains (Name, In_Layer, True),
     Post =>
       Table.Contains (Name, In_Layer, True)
     and Table.Current_Layer = Table.Current_Layer'Old
     and Table.Find (Name, In_Layer, True) = Position
     and Has_Value (Position)
     and Element (Position) = Initial_Value;

   procedure Create
     (Table         : in out Map;
      Name          : Key_Type;
      In_Layer      : Layer_ID;
      Initial_Value : Element_Type)
     with
       Pre =>
         not Table.Contains (Name, In_Layer, True),
     Post =>
       Table.Current_Layer = Table.Current_Layer'Old
       and Table.Contains (Name, In_Layer, True)
     and Element (Table.Find (Name, In_Layer, True)) = Initial_Value;




   procedure Update (Table     : in out Map;
                     Key       : Key_Type;
                     New_Value : Element_Type;
                     In_Layer  : Layer_ID)
     with
       Pre => Table.Contains (Name       => Key,
                              In_Layer   => In_Layer,
                              Local_Only => True),
     Post =>
       Table.Contains (Key, In_Layer, True)
     and then
       Element (Table.Find (Key, In_Layer, True)) = New_Value;

   procedure Update (Table     : in out Map;
                     Key       : Key_Type;
                     New_Value : Element_Type)
     with
       Pre => Table.Contains (Key, Table.Current_Layer, True),
     Post =>
       Table.Contains (Key, Table.Current_Layer, True)
     and then
       Element (Table.Find (Key, Table.Current_Layer, True)) = New_Value;
   -- Syntactic sugar.  Like the 4-parameter update, but the current layer
   -- is implicit

   procedure Update (Pos       : Cursor;
                     New_Value : Element_Type)
     with
       Pre => Pos /= No_Element,
       Post => Element (Pos) = New_Value;

   procedure Put (Table      : in out Map;
                  Key        : Key_Type;
                  New_Value  : Element_Type;
                  In_Layer   : Layer_ID;
                  Position   : out Cursor;
                  Local_Only : Boolean := False)
     with
       Post =>
         Table.Contains (Key, In_Layer, Local_Only)
     and Table.Find (Key, In_Layer, Local_Only) = Position
     and Element (Position) = New_Value
     and (if not Table.Contains (Key, In_Layer, Local_Only)'Old then
              Layer_Of (Position) = In_Layer);
   -- If Key is in In_Layer or one of the ancestors of In_Layer
   -- (if Local_Only = False), Update the corresponding entry;
   -- otherwise create an entry in In_Layer (always, even if
   -- Local_Only = True) and write New_Value to it.

   procedure Put (Table      : in out Map;
                  Key        : Key_Type;
                  New_Value  : Element_Type;
                  In_Layer   : Layer_ID;
                  Local_Only : Boolean := False)
     with
       Post =>
         Table.Contains (Key, In_Layer, Local_Only)
     and Element (Table.Find (Key, In_Layer, Local_Only)) = New_Value;


   procedure Put (Table      : in out Map;
                  Key        : Key_Type;
                  New_Value  : Element_Type;
                  Position   : out Cursor;
                  Local_Only : Boolean := False)
     with
       Post =>
         Table.Contains (Key, Table.Current_Layer, Local_Only)
     and Table.Find (Key, Table.Current_Layer, Local_Only) = Position
     and Element (Position) = New_Value;


   procedure Put (Table      : in out Map;
                  Key        : Key_Type;
                  New_Value  : Element_Type;
                  Local_Only : Boolean := False)
     with
       Post =>
         Table.Contains (Key, Table.Current_Layer, Local_Only)
     and Element (Table.Find (Key, Table.Current_Layer, Local_Only)) = New_Value;

   procedure Delete (Table : in out Map;
                     Pos   : in out Cursor)
     with
       Pre => not Is_Stale (Pos),
     Post => Pos = No_Element;


   package Printing is
      --
      -- Useful for debugging.  The user can setup functions that convert
      -- keys and values to string.  This is used in generating debug
      -- and exception messages.
      --
      -- Why specifying the converter in this way  and not as a parameter
      -- to package?  Because this is a feature that it is not always
      -- required.
      --
      -- If a conversion function is not specified a generic "<element>"
      -- or "<key>" will be printed.
      --

      type Value_Image is access function (X : Element_Type) return String;
      type Key_Image is access function (X : Key_Type) return String;

      procedure Set_Printers (Value_To_String : Value_Image := null;
                              Key_To_String   : Key_Image := null);
      function Image (X : Element_Type) return String;
      function Image (X : Key_Type) return String;
   end Printing;

   Stale_Cursor         : exception;
   Uninitialized_Value  : exception;
   Layer_Table_Mismatch : exception;
private
   --
   --
   -- The structure of the symbol table is as follows:
   --
   -- * We have a stacks of layers
   --   * The top of the stack is the current layer.
   --   * When a new layer is open, it is pushed on the stack, when it
   --     is closed it is popped out
   --   * The stack can never be empty, the bottom of the stack is
   --     the root layer.
   --
   -- * Every layer (but the root) has  a "parent" that can be searched
   --   when the symbol is not found.
   --
   -- * Every layer has an ID that is *monotonically increased*.  Two
   --   namespaces will **never** have the same ID.  This ID is not
   --   stricly necessary, but it is useful to check for stale Cursors.
   --
   -- * Every layer has two structures
   --   * a vector of records holding (Key, Element) pairs and
   --   * a Map mapping keys to positions in the above vector
   --
   -- Why this involuted structure?  Why not just a map sending names
   -- to values?  Because in this way we can have a Cursor stable
   -- against new additions to the table, a feature that is sometimes
   -- required and that maybe is not guaranteed with the usual
   -- library structures.
   --
   -- * The Cursor has
   --   * An access to the table structure
   --   * The stack index of the corresponding layer
   --   * The index of the entry in the Element vector
   --   * The layer ID (used to check freshness)
   --
   -- Even if new entries are added to the table, a Cursor will still
   -- "point to" the same entry.  The only way to make a Cursor invalid
   -- is by deleting the corresponding layer (that will make the Cursor
   -- stale)
   --
   -- This Cursor is
   --

   type Extended_Value_Index is range 1 .. Positive'Last;
   subtype Value_Index is
     Extended_Value_Index range 1 .. Extended_Value_Index'Last - 1;

   No_Value             : constant Extended_Value_Index := Extended_Value_Index'Last;

   type Map_Number is range 1 .. Positive'Last;
   type Unique_Monotone_ID is range 0 .. Positive'Last;

   Root_ID    : constant Unique_Monotone_ID := Unique_Monotone_ID'First;
   Root_Index : constant Map_Number := Map_Number'First;

   package Name_Maps is
     new  Ada.Containers.Indefinite_Hashed_Maps
       (Key_Type        => Key_Type,
        Element_Type    => Value_Index,
        Hash            => Hash,
        Equivalent_Keys => Equivalent_Names);

   package Value_Holders is
     new Ada.Containers.Indefinite_Holders (Element_Type);

   package Name_Holders is
     new Ada.Containers.Indefinite_Holders (Key_Type);

   type Entry_Data is
      record
         Name  : Name_Holders.Holder;
         Value : Value_Holders.Holder;
      end record
     with
       Dynamic_Predicate =>
         (if Entry_Data.Name.Is_Empty then Entry_Data.Value.Is_Empty);

   function Is_Stale (Item : Entry_Data) return Boolean
   is (Item.Name.Is_Empty);

   Stale_Entry : constant Entry_Data :=
                   (Name => Name_Holders.Empty_Holder,
                    Value => Value_Holders.Empty_Holder);

   package Entry_Vectors is
     new Ada.Containers.Indefinite_Vectors (Index_Type   => Value_Index,
                                            Element_Type => Entry_Data);

   type Basic_Table;
   type Basic_Table_Access is access Basic_Table;

   type Layer_ID is
      record
         Table : Basic_Table_Access;
         Index : Map_Number;
         ID    : Unique_Monotone_ID;
      end record;

   No_Layer : constant Layer_ID := (Table => null,
                                    Index => Map_Number'Last,
                                    ID    => Unique_Monotone_ID'Last);

   type Cursor is
      record
         Layer : Layer_ID;
         Idx   : Extended_Value_Index;
      end record;

   No_Element : constant Cursor := Cursor'(Layer => No_Layer,
                                           Idx   => No_Value);


   function Layer_Of (Pos : Cursor) return Layer_ID
   is (Pos.Layer);

   type Layer_Type is
      record
         Name_Map : Name_Maps.Map;
         Entries  : Entry_Vectors.Vector;
         ID       : Unique_Monotone_ID;
         Parent   : Layer_ID;
      end record;

   package Stacks_Of_Layers is
     new Ada.Containers.Vectors (Index_Type   => Map_Number,
                                 Element_Type => Layer_Type);

   subtype Layer_Stack is Stacks_Of_Layers.Vector;

   Empty_Stack : constant Layer_Stack := Stacks_Of_Layers.Empty_Vector;

   procedure Push (Stack : in out Layer_Stack;
                   Item  : Layer_Type);

   procedure Pop (Stack : in out Layer_Stack);

   type Basic_Table is
      record
         Stack : Stacks_Of_Layers.Vector;
      end record;

   type Map is
     new Finalization.Limited_Controlled
   with
      record
         Counter : Unique_Monotone_ID := Root_ID;
         T       : Basic_Table_Access;
      end record;

   overriding procedure Initialize (Object : in out Map);

   function Root (T : Map) return Layer_ID
   is (Layer_ID'(Table => T.T,
                 Index => Root_Index,
                 ID    => Root_ID));

   function Current_Layer (T : Map) return Layer_ID
   is (Layer_ID'(Table => T.T,
                 Index => T.T.Stack.Last_Index,
                 Id    => T.T.Stack.Last_Element.ID));

   function To_Layer (Id : Layer_Id) return Layer_Type
   is (Id.Table.Stack.Element (Id.Index));

   function Parent_Of (Id : Layer_ID) return Layer_ID
   is (To_Layer (Id).Parent);


   function Contains (Layer : Layer_ID;
                      Name  : Key_Type)
                      return Boolean
   is (Layer.Table.Stack (Layer.Index).Name_Map.Contains (Name));

   function Data_Of (Pos : Cursor) return Entry_Data
   is (if Is_Stale (Pos) then
          raise Stale_Cursor
       else
          Pos.Layer.Table.Stack (Pos.Layer.Index).Entries (Pos.Idx));

   function Is_Stale (Pos : Cursor) return Boolean
   is (Pos.Layer /= No_Layer and then
         (
          (Pos.Layer.Index > Pos.Layer.Table.Stack.Last_Index)
          or else
            (Pos.Layer.Table.Stack (Pos.Layer.Index).Id /= Pos.Layer.Id)
          or else
            (Is_Stale (Data_Of (Pos)))
         )
      );






   function Has_Value (Pos : Cursor) return Boolean
   is (Pos.Layer /= No_Layer
       and then not Is_Stale (Pos)
       and then not Value_Holders.Is_Empty (Data_Of (Pos).Value));

   function Key (Pos : Cursor) return Key_Type
   is (Name_Holders.Element (Data_Of (Pos).Name));

   function Parent_Of (T     : Map;
                       Layer : Layer_ID) return Layer_ID
   is (Layer_ID'(Table => Layer.Table,
                 Index => To_Layer (Layer).Parent.Index,
                 ID    => To_Layer (Layer).Parent.ID));

end Layered_Maps.Hashed_Maps;
