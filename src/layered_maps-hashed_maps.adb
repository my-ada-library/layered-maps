pragma Ada_2012;

with Ada.Text_IO; use Ada.Text_IO;

package body Layered_Maps.Hashed_Maps is
   Activate_Debug : constant Boolean := False;

   package body Printing is
      Value_Printer : Value_Image := null;
      Key_Printer   : Key_Image := null;

      procedure Set_Printers (Value_To_String : Value_Image := null;
                              Key_To_String   : Key_Image := null)
      is
      begin
         Value_Printer := Value_To_String;
         Key_Printer := Key_To_String;
      end Set_Printers;

      function Image (X : Element_Type) return String
      is (if Value_Printer = null then
             "<element>"
          else
             Value_Printer (X));

      function Image (X : Key_Type) return String
      is (if Key_Printer = null then
             "<key>"
          else
             Key_Printer (X));
   end Printing;


   procedure Debug (X : String) is
   begin
      if Activate_Debug then
         Put_Line (Standard_Error, X);
      end if;
   end Debug;


   -----------------
   -- Is_Ancestor --
   -----------------

   function Is_Ancestor (Ancestor   : Layer_ID;
                         Descendant : Layer_ID)
                         return Boolean
   is
      Position : Layer_ID := Descendant;
   begin
      while Position /= No_Layer loop
         if Position = Ancestor then
            return True;
         end if;

         Position := Parent_Of (Position);
      end loop;

      return False;
   end Is_Ancestor;

   procedure Push (Stack : in out Layer_Stack;
                   Item  : Layer_Type)
   is
   begin
      Stack.Append (Item);
   end Push;

   procedure Pop (Stack : in out Layer_Stack)
   is
   begin
      Stack.Delete_Last;
   end Pop;



   function Copy_Globals (T : Map) return Map
   is
      use Ada.Finalization;

   begin
      return Result : constant Map :=
        Map'(Limited_Controlled with T => new Basic_Table,
             Counter                   => <>)
      do
         Result.T.Stack.Append (T.T.Stack.First_Element);
      end return;
   end Copy_Globals;

   -------------
   -- Next_Id --
   -------------

   function Next_Id (Table : in out Map) return Unique_Monotone_ID
   is
   begin
      Table.Counter := Table.Counter + 1;
      return Table.Counter;
   end Next_Id;

   --------------------
   -- Open_New_Layer --
   --------------------

   procedure Open_New_Layer (Table : in out Map; Parent : Layer_ID) is
      New_Block : constant Layer_Type :=
                    Layer_Type'(Name_Map => Name_Maps.Empty_Map,
                                Entries  => Entry_Vectors.Empty_Vector,
                                ID       => Next_Id (Table),
                                Parent   => Parent);

   begin
      Push (Table.T.Stack, New_Block);
   end Open_New_Layer;


   --------------------
   -- Open_New_Layer --
   --------------------

   procedure Open_New_Layer (Table     : in out Map;
                             Parent    : Layer_ID;
                             New_Layer : out Layer_ID)
   is
   begin
      Table.Open_New_Layer (Parent);
      New_Layer := Table.Current_Layer;
   end Open_New_Layer;



   -------------------------
   -- Close_Current_Layer --
   -------------------------

   procedure Close_Current_Layer (Table : in out Map)
   is
      use Ada.Containers;
   begin
      --        Debug ("Closing block " & Table.Current.Id'Image);

      if Table.T.Stack.Length = 1 then
         raise Constraint_Error;
      end if;

      Pop (Table.T.Stack);
   end Close_Current_Layer;

   ----------
   -- Find --
   ----------

   function Find (Table      : Map;
                  Name       : Key_Type;
                  In_Layer   : Layer_ID;
                  Local_Only : Boolean := False)
                  return Cursor
   is
      use type Name_Maps.Cursor;

      Pos           : Name_Maps.Cursor;
      Current_Trial : Map_Number := In_Layer.Index;
   begin
      if In_Layer.Table /= Table.T then
         raise Layer_Table_Mismatch;
      end if;

      loop
         Debug ("Looking for '" & Printing.Image (Name) & "' in block " & Current_Trial'Image);

         Pos := Table.T.Stack (Current_Trial).Name_Map.Find (Name);

         if Pos /= Name_Maps.No_Element then
            Debug ("found");
            return Cursor'(Idx       => Name_Maps.Element (Pos),
                           Layer     =>
                             Layer_ID'(Table => Table.T,
                                       Index => Current_Trial,
                                       ID    => Table.T.Stack (Current_Trial).Id));
         end if;

         exit when Current_Trial = Root_Index or Local_Only;

         Current_Trial := Table.T.Stack (Current_Trial).Parent.Index;
      end loop;

      Debug ("Not found");
      return No_Element;
   end Find;

   ------------
   -- Create --
   ------------

   procedure Create
     (Table         : in out Map;
      Name          : Key_Type)
   is
      Ignored : Cursor;
   begin
      Table.Create (Name          => Name,
                    Position      => Ignored);
   end Create;

   ------------
   -- Create --
   ------------

   procedure Create
     (Table         : in out Map;
      Name          : Key_Type;
      In_Layer      : Layer_ID;
      Position      : out Cursor)
   is
      Ignored : Boolean;
      Pos     : constant Map_Number := In_Layer.Index;
      Idx     : Value_Index;
   begin
      Debug ("Creating '"
             & Printing.Image (Name)
             & "' in block ID="
             & Table.T.Stack (Pos).Id'Image);
      declare
         New_Entry : constant Entry_Data :=
                       (Name  => Name_Holders.To_Holder (Name),
                        Value => Value_Holders.Empty_Holder);
      begin
         Table.T.Stack (Pos).Entries.Append (New_Entry);
      end;

      Idx := Table.T.Stack (Pos).Entries.Last_Index;

      Table.T.Stack (Pos).Name_Map.Insert (Key      => Name,
                                           New_Item => Idx);

      Position :=
        Cursor'(Idx       => Idx,
                Layer     => Layer_ID'(Table => Table.T,
                                       Index => Pos,
                                       ID    => Table.T.Stack (Pos).Id));
   end Create;

   procedure Create
     (Table         : in out Map;
      Name          : Key_Type;
      In_Layer      : Layer_ID;
      Initial_Value : Element_Type;
      Position      : out Cursor)
   is
   begin
      Table.Create (Name     => Name,
                    In_Layer => In_Layer,
                    Position => Position);

      Update (Position, Initial_Value);
   end Create;

   procedure Create
     (Table         : in out Map;
      Name          : Key_Type;
      In_Layer      : Layer_ID;
      Initial_Value : Element_Type)
   is
      Ignored : Cursor;
   begin
      Table.Create (Name          => Name,
                    In_Layer      => In_Layer,
                    Initial_Value => Initial_Value,
                    Position      => Ignored);
   end Create;

   procedure Create
     (Table         : in out Map;
      Name          : Key_Type;
      Initial_Value : Element_Type)
   is
      Ignored : Cursor;
   begin
      Table.Create (Name          => Name,
                    In_Layer      => Table.Current_Layer,
                    Initial_Value => Initial_Value,
                    Position      => Ignored);
   end Create;

   procedure Create
     (Table         : in out Map;
      Name          : Key_Type;
      In_Layer      : Layer_ID)
   is
      Ignored : Cursor;
   begin
      Table.Create (Name     => Name,
                    In_Layer => Table.Current_Layer,
                    Position => Ignored);
   end Create;


   procedure Create
     (Table         : in out Map;
      Name          : Key_Type;
      Position      : out Cursor)
   is
   begin
      Table.Create (Name     => Name,
                    In_Layer => Table.Current_Layer,
                    Position => Position);
   end Create;

   procedure Create (Table         : in out Map;
                     Name          : Key_Type;
                     Initial_Value : Element_Type;
                     Position      : out Cursor)
   is
   begin
      Table.Create (Name          => Name,
                    In_Layer      => Table.Current_Layer,
                    Initial_Value => Initial_Value,
                    Position      => Position);
   end Create;

   ------------
   -- Update --
   ------------

   procedure Update (Pos       : Cursor;
                     New_Value : Element_Type)
   is

   begin
      if Pos = No_Element  then
         raise Constraint_Error;
      end if;

      if Is_Stale (Pos) then
         raise Stale_Cursor;
      end if;

      Debug ("Updating '"
             & Printing.Image (Key (Pos))
             & "' in block "
             & Pos.Layer.ID'Image
             & " to [" & Printing.Image (New_Value) & "]");

      declare
         Working_Layer : constant Layer_ID := Pos.Layer;
         Working_Table : constant Basic_Table_Access := Working_Layer.Table;

         Old_Entry : constant Entry_Data :=
                       Working_Table.Stack (Pos.Layer.Index).Entries (Pos.Idx);

         New_Entry : constant Entry_Data :=
                       (Name  => Old_Entry.Name,
                        Value => Value_Holders.To_Holder (New_Value));
      begin
         Working_Table
           .Stack (Pos.Layer.Index)
           .Entries (Pos.Idx) := New_Entry;
      end;
   end Update;

   ------------
   -- Update --
   ------------

   procedure Update (Table     : in out Map;
                     Key       : Key_Type;
                     New_Value : Element_Type;
                     In_Layer  : Layer_ID)
   is
      Pos : constant Cursor := Table.Find (Name       => Key,
                                           In_Layer   => In_Layer,
                                           Local_Only => True);
   begin
      if Pos = No_Element then
         raise Constraint_Error with "missing key " & Printing.Image (Key);
      end if;

      Update (Pos, New_Value);
   end Update;

   ------------
   -- Update --
   ------------

   procedure Update (Table     : in out Map;
                     Key       : Key_Type;
                     New_Value : Element_Type)
   is
   begin
      Table.Update (Key       => Key,
                    New_Value => New_Value,
                    In_Layer  => Table.Current_Layer);
   end Update;


   ---------
   -- Put --
   ---------

   procedure Put (Table      : in out Map;
                  Key        : Key_Type;
                  New_Value  : Element_Type;
                  In_Layer   : Layer_ID;
                  Position   : out Cursor;
                  Local_Only : Boolean := False)
   is
   begin
      Position := Table.Find (Name       => Key,
                              In_Layer   => In_Layer,
                              Local_Only => Local_Only);

      if Position = No_Element then
         --
         -- No entry for key was found. Put requirements ask that we
         -- create a new entry in the starting layer
         --
         Table.Create (Name     => Key,
                       In_Layer => In_Layer,
                       Position => Position);
      end if;

      --
      -- Here Position always point to the right entry. We can
      -- use Update
      --

      pragma Assert (Position /= No_Element);
      Update (Pos       => Position,
              New_Value => New_Value);
   end Put;

   ---------
   -- Put --
   ---------

   procedure Put (Table      : in out Map;
                  Key        : Key_Type;
                  New_Value  : Element_Type;
                  In_Layer   : Layer_ID;
                  Local_Only : Boolean := False)
   is
      Ignored : Cursor;
   begin
      Table.Put (Key        => Key,
                 New_Value  => New_Value,
                 In_Layer   => In_Layer,
                 Position   => Ignored,
                 Local_Only => Local_Only);
   end Put;

   ---------
   -- Put --
   ---------

   procedure Put (Table      : in out Map;
                  Key        : Key_Type;
                  New_Value  : Element_Type;
                  Position   : out Cursor;
                  Local_Only : Boolean := False)
   is
   begin
      Table.Put (Key        => Key,
                 New_Value  => New_Value,
                 In_Layer   => Table.Current_Layer,
                 Position   => Position,
                 Local_Only => Local_Only);
   end Put;


   ---------
   -- Put --
   ---------

   procedure Put (Table      : in out Map;
                  Key        : Key_Type;
                  New_Value  : Element_Type;
                  Local_Only : Boolean := False)
   is
      Ignored : Cursor;
   begin
      Table.Put (Key        => Key,
                 New_Value  => New_Value,
                 In_Layer   => Table.Current_Layer,
                 Position   => Ignored,
                 Local_Only => Local_Only);
   end Put;


   procedure Delete (Table : in out Map;
                     Pos   : in out Cursor)
   is
   begin
      --
      -- In order to remove the given entry we
      --
      --  1. Remove the key from the key -> index map
      --  2. Make stale the corresponding entry in the entry vector
      --
      -- Note that the entry in the vector is not removed, but just
      -- overwritten with Stale_Entry.  In this way existing cursors
      -- are not invalidated (unless they "point" to the deleted entry)
      --
      if Table.T /= Pos.Layer.Table then
         raise Constraint_Error;
      end if;

      if Is_Stale (Pos) then
         raise Stale_Cursor;
      end if;

      declare
         P   :  Name_Maps.Cursor  :=
                 Pos.Layer.Table.Stack (Pos.Layer.Index)
                 .Name_Map.Find (Key (Pos));
      begin
         pragma Assert (Name_Maps.Has_Element (P));

         Pos.Layer.Table.Stack (Pos.Layer.Index).Name_Map.Delete (P);
      end;

      Pos.Layer.Table.Stack (Pos.Layer.Index).Entries (Pos.Idx) := Stale_Entry;

      Pos := No_Element;
   end Delete;

   ----------------
   -- Initialize --
   ----------------

   overriding procedure Initialize (Object : in out Map) is
   begin

      Object.Counter := Root_ID;
      Object.T := new Basic_Table'(Stack => Empty_Stack);

      Push (Stack => Object.T.Stack,
            Item  => Layer_Type'(Name_Map => Name_Maps.Empty_Map,
                                 Entries  => Entry_Vectors.Empty_Vector,
                                 ID       => Root_ID,
                                 Parent   => No_Layer));
   end Initialize;

   -------------
   -- Element --
   -------------

   function Element (Pos : Cursor) return Element_Type
   is
   begin
      if Is_Stale (Pos) then
         raise Stale_Cursor;
      end if;

      if not Has_Value (Pos) then
         raise Uninitialized_Value with Printing.Image (Key (Pos));
      end if;

      return Value_Holders.Element (Data_Of (Pos).Value);
   end Element;

   function Image (X : Cursor) return String
   is (if Has_Value (X) then
          "["
       & X.Idx'Image
       & "@"
       & X.Layer.Index'Image
       & ","
       & X.Layer.ID'Image
       & "]"
       else
          "NO_ELEMENT");

end Layered_Maps.Hashed_Maps;
