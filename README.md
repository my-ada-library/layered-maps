# What is this?

This is an Ada library that implements a *layered map*, a structure that can be useful in the implementation of interpreters and compilers in order to handle local/global variables/functions.

## The conceptual model of a layered map

The model is as follows: a layered map is initially a simple map (*root map* or *global map*) that allows to store `(key, value)` pairs.  In a layered map, however, a new map can be layered above the current one and the new map becomes the *current map*. When a `key` is searched first the top map is searched, then the map below is searched and so on until the `key` is found or the *root map* is reached.  

It is clear that topmost maps hide lower level ones.  The *current map* can be deleted and in this case the previous *current map* is restored. 

More formally,
* A *layered map* is a **tree** of maps.  It always had at least one map: the *root map* or *global map* (the symbols stored there is seen everywhere)
* A new map can be *layered* above an existing map.  Borrowing the language from trees, we will call the new map a *child* of the old one, while the old map is the *parent* of the new one.
* At any time there is a *current map* that is also a *top level map*, that is, it has no children.  At the begininning the *current map* is the *root map*. 
* When a new map is added it becomes the new *current map*-
* Every map (but the *root*) remember the map that was current when the map has been created, the *old current map*.  When the new map is deleted the previous current map is restored.
* When the layered map is queried for a  `key`, first the *current map* is searched for, then its parent and so on until the `key` is found or the *root* is reached.

# The API

The library is provided as a generic package with an interface similar to  `Indefinite_Hashed_Maps`.  

## The main type: the `Map`
The main exported type is `Map` (still similarly to `Indefinite_Hashed_Maps`).  

> Differently from the usual hashed maps, a key can exist in the map without an associated value.  This is similar to declaring a variable without initializing it. 

The operations that can be done on a map can be collected in wide classes
* **Layer** related operations 
  * Creating a new layer (`Open_New_Layer`)
  * Closing the current layer (`Close_Current_Layer`)
  * Getting the current layer or the root 
    * `Current_Layer`
    * `Root`
  * Finding the parent of a given layer
    * `Parent_Of`
    * `Is_Ancestor`
* **Searching** for a key and getting the corresponding value 
  * `Contains`
  * `Find`
  * `Element`, `Key` 
* **Writing** to the table by creating a key and/or updating the corresponding value
  * `Create`  creates an entry for a key with an unassigned value
  * `Update`  updates the value associated with a key
  * `Put` this is the most similar to the behaviour of many languages: if the key exists, update it, otherwise create a new entry and assign it the value
* **Deleting** entries
  * `Delete`

Check the spec file for details about each subprogram.

Every operation exists in many "flavors." For example, some flavors of `Find` search by default in the current layer (the most common case), while others allows to search in any layer. As another example, some versions of `Put` returns a `Cursor` to the just updated/created variable, other versions do not return it.  Check the spec file for details.

## Cursors

Similarly to other Ada containers, this package define a `Cursor` type that points to an entry in the table.  Differently from other cursors, however, the provided cursor is "stable" with respect to data addition and update to the table. That is, adding new entries or modifying old ones do not invalidate existing cursors. The only case when a cursor gets invalid (*stale*) is when the corresponding layre or the corresponding entry are deleted.
